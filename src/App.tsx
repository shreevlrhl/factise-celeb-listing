// App.tsx

import React, { useState } from 'react';
import Task from './components/Task';
import './styles/App.scss'; // Import the CSS file


const App: React.FC = () => {

  return (
    <Task />
  );
};

export default App;