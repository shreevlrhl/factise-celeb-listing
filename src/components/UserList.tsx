import React, { useState } from "react";
import Modal from "react-modal";

import {
  IoIosArrowDown,
  IoIosArrowUp,
  IoIosCreate,
  IoIosTrash,
} from "react-icons/io";
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa";
import userData from "../celebrities.json";
import { calculateAge } from "../utils/Utils";
import "../styles/UserList.scss";

interface UserListProps {
  onSelect: (userId: number) => void;
  selectedUser: number | null;
}

interface User {
  id: number;
  first: string;
  last: string;
  dob: string;
  gender: string;
  country: string;
  description: string;
  picture: string;
}

const uData = userData;
const UserList: React.FC<UserListProps> = ({ onSelect, selectedUser }) => {
  const [userData, setUserData] = useState<User[]>(uData);
  const [activePanel, setActivePanel] = useState<number | null>(null);
  const [editState, setEditState] = useState<number | null>(null);
  const [editedValues, setEditedValues] = useState<Partial<User>>({});
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [isChanged, setIsChanged] = useState(false);
  const [userToDelete, setUserToDelete] = useState<number | null>(null);
  const [isDeleteModalOpen, setDeleteModalOpen] = useState<boolean>(false);
  const [dobError, setDobError] = useState<string | null>(null);
  const [nationalityError, setNationalityError] = useState<string | null>(null);
  const [nameError, setNameError] = useState<string | null>(null);
  const [genderError, setGenderError] = useState<string | null>(null);
  const [descriptionError, setDescriptionError] = useState<string | null>(null);

  const handleAccordionToggle = (userId: number) => {
    if (editState !== null) {
      // If in edit mode, do not toggle another accordion
      return;
    }
    setActivePanel((prevActivePanel) =>
      prevActivePanel === userId ? null : userId
    );
    setEditState(null);
    setEditedValues({});
  };

  const handleEditClick = (userId: number) => {
    setEditState(userId);
    setEditedValues({});
  };

  const handleEditChange = (field: string, value: string) => {
    setEditedValues((prevValues) => {
      let updatedValues: Partial<User> = { ...prevValues };
      if (field === "name") {
        const [first, last] = value.split(" ");
        updatedValues = { ...updatedValues, first, last };
      } else if (field === "dob") {
        const birthYear = new Date().getFullYear() - parseInt(value, 10);
        updatedValues = { ...updatedValues, dob: `${birthYear}-01-01` };
        // Validation for Date of Birth
        if (isNaN(birthYear) || value.trim() === "") {
          setDobError("Please enter a valid age. Age should be a number");
          return prevValues;
        }
      } else {
        updatedValues = { ...updatedValues, [field]: value };
      }

      if (field === "nationality") {
        if (value.trim() === "") {
          setNationalityError(
            "Nationality must not be empty and should not contain numbers"
          );
        }
        return prevValues;
      }

      if (value.trim() === "") {
        switch (field) {
          case "name":
            setNameError(`${value} must not be empty`);
          case "nationality":
            setNationalityError(`${value} must not be empty`);
          case "age":
            setDobError(`${value} must not be empty`);
          case "gender":
            setGenderError(`${value} must not be empty`);
          case "description":
            setDescriptionError(`${value} must not be empty`);
        }
        return prevValues;
      }

      setIsChanged(true);
      return updatedValues;
    });
  };

  const handleSaveClick = (userId: number) => {
    setUserData((prevUserData) =>
      prevUserData.map((u) =>
        u.id === editState
          ? {
              ...u,
              first: editedValues?.first || u?.first,
              last: editedValues?.last || u?.last,
              dob: editedValues?.dob || u?.dob,
              age:
                calculateAge(editedValues.dob || u.dob) || calculateAge(u.dob),
              country: editedValues.country || u.country,
              description: editedValues.description || u.description,
              gender: editedValues.gender || u.gender,
            }
          : u
      )
    );
    setEditState(null);
    setIsChanged(false);
  };

  const handleCancelClick = (userId: number) => {
    // Handle cancel functionality, discard the edited values
    console.log(`Cancel button clicked for user with ID: ${userId}`);
    setEditState(null);
    setEditedValues({});
  };

  const handleDeleteClick = (userId: number) => {
    setDeleteModalOpen(true);
    setUserToDelete(userId);
    console.log(`Delete button clicked for user with ID: ${userId}`);
  };

  const handleConfirmDelete = () => {
    if (userToDelete !== null) {
      const updatedUserData = userData.filter(
        (user) => user.id !== userToDelete
      );
      setUserData(updatedUserData);
      setUserToDelete(null);
      setDeleteModalOpen(false);
    }
  };

  const handleCancelDelete = () => {
    setUserToDelete(null);
    setDeleteModalOpen(false);
  };

  const filteredUsers = userData.filter(
    (user) =>
      user.first.toLowerCase().includes(searchTerm.toLowerCase()) ||
      user.last.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="container">
      <input
        type="text"
        className="form-control search-input search"
        placeholder="Search"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <div id="userAccordion" className="accordion">
        {filteredUsers.map((user) => (
          <div
            className={`card ${activePanel === user.id ? "active" : ""} mb-3`}
            key={user.id}
          >
            <div
              className="card-header full-width-card d-flex align-items-center justify-content-between"
              onClick={() => handleAccordionToggle(user.id)}
            >
              <img
                src={user.picture}
                alt={`${user.first} ${user.last}`}
                className="user-picture rounded-circle shadow-4-strong"
              />
              <>
              {editState === user.id ? (
                <input
                  type="text"
                  className="pl-2 form-control rounded-lg"
                  value={`${editedValues.first || user.first} ${
                    editedValues.last || user.last
                  }`}
                  onChange={(e) => handleEditChange("name", e.target.value)}
                />
              ) : (
                <span className="u-name">
                  {user.first} {user.last}
                </span>
              )}
              {nameError && <small className="text-danger">{nameError}</small>}
              </>

              {activePanel === user.id ? (
                <IoIosArrowUp className="ml-6" />
              ) : (
                <IoIosArrowDown className="ml-6" />
              )}
            </div>
            {activePanel === user.id && (
              <div className="card-body">
                <div className="row">
                  {/* <p>ID: {user.id}</p> */}
                  <div className="form-group col-md-4 pt-1">
                    <label className="control-label">Age</label>
                    {editState === user.id ? (
                        <input
                          className="form-control"
                          type="text"
                          value={
                            editedValues.dob
                              ? calculateAge(editedValues.dob)
                              : calculateAge(user.dob)
                          }
                          onChange={(e) =>
                            handleEditChange("dob", e.target.value)
                          }
                        />
                    ) : (
                      <div className="col">{calculateAge(user.dob)} Years</div>
                    )}
                    {dobError && <small className="text-danger">{dobError}</small>}
                  </div>
                  <div className="form-group col-md-4 pt-1">
                    <label htmlFor="gender" className="control-label">
                      Gender
                    </label>
                    {editState === user.id ? (
                      <select
                        id="gender"
                        style={{ appearance: "auto" }}
                        className="form-control mb-3"
                        value={editedValues.gender || user.gender}
                        onChange={(e) =>
                          handleEditChange("gender", e.target.value)
                        }
                      >
                        <option value="">Select gender</option>
                        <option value="male" selected={user.gender === "male"}>
                          Male
                        </option>
                        <option
                          value="female"
                          selected={user.gender === "female"}
                        >
                          Female
                        </option>
                        <option
                          value="transgender"
                          selected={user.gender === "transgender"}
                        >
                          Transgender
                        </option>
                        <option
                          value="rather not say"
                          selected={user.gender === "rather not say"}
                        >
                          Rather not say
                        </option>
                        <option
                          value="other"
                          selected={user.gender === "other"}
                        >
                          Other
                        </option>
                      </select>
                    ) : (
                      <div className="col">{user.gender}</div>
                    )}
                    {genderError && <small className="text-danger">{genderError}</small>}
                  </div>
                  <div className="form-group col-md-4 pt-1">
                    <label className="control-label">Country</label>
                    {editState === user.id ? (
                      <input
                        className="form-control"
                        type="text"
                        value={editedValues.country || user.country}
                        onChange={(e) =>
                          handleEditChange("country", e.target.value)
                        }
                      />
                    ) : (
                      <div className="col">{user.country}</div>
                    )}
                    {nationalityError && (
                      <small className="text-danger">{nationalityError}</small>
                    )}
                  </div>
                  <div className="form-group col-md-12 pt-3 mt-2">
                    <label className="control-label">Description</label>
                    {editState === user.id ? (
                      <textarea
                        className="form-control rounded-lg"
                        value={editedValues.description || user.description}
                        onChange={(e) =>
                          handleEditChange("description", e.target.value)
                        }
                      />
                    ) : (
                      <div className="col">{user.description}</div>
                    )}
                    {descriptionError && <small className="text-danger">{descriptionError}</small>}
                  </div>
                  {editState === user.id && (
                    <div className="d-flex mt-3 justify-content-end">
                      <FaCheckCircle
                        className={`cross-icon  fs-4 ${
                          !isChanged ? "text-secondary" : "text-success"
                        }`}
                        onClick={() => isChanged && handleSaveClick(user.id)}
                      />
                      <FaTimesCircle
                        className="times-circle text-danger fs-4"
                        onClick={() => handleCancelClick(user.id)}
                      />
                    </div>
                  )}
                  {userToDelete !== null && (
                    <Modal
                      isOpen={isDeleteModalOpen}
                      onRequestClose={handleCancelDelete}
                      style={{
                        overlay: {
                          backgroundColor: "rgba(0, 0, 0, 0.5)",
                        },
                        content: {
                          width: "30%", // Adjust as needed
                          height: "20%", // Adjust as needed
                          margin: "auto",
                        },
                      }}
                    >
                      <div>
                        <textarea
                          className="pl-1"
                          style={{ width: "95%", height: "70%" }}
                          placeholder="Please provide a reason for deletion..."
                          value="Are you sure you want to delete this user ?"
                        />
                        <div className="d-flex mt-3 justify-content-end">
                          <button onClick={handleConfirmDelete}>Delete</button>
                          <button onClick={handleCancelDelete}>Cancel</button>
                        </div>
                      </div>
                    </Modal>
                  )}
                  <div className="d-flex mt-3 justify-content-end">
                    <IoIosTrash
                      className="delete-icon text-danger fs-5"
                      onClick={() => handleDeleteClick(user.id)}
                    />

                    <IoIosCreate
                      className="edit-icon text-primary fs-5 ml-3"
                      style={{
                        backgroundColor:
                          calculateAge(user.dob ?? editedValues.dob ?? "") >= 18
                            ? "transparent"
                            : "#dddddd",
                      }}
                      onClick={() =>
                        calculateAge(user.dob ?? editedValues.dob ?? "") >= 18
                          ? handleEditClick(user.id)
                          : ""
                      }
                    />
                  </div>
                </div>{" "}
                {/* formrow */}
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserList;
