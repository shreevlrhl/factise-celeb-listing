// App.tsx

import React, { useState } from 'react';
import UserList from './UserList';

const App: React.FC = () => {
  const [selectedUser, setSelectedUser] = useState<number | null>(null);

  const handleUserSelect = (userId: number) => {
    setSelectedUser((prevUserId) => (prevUserId === userId ? null : userId));
  };

  return (
    // <div className='container'>
      <div className='center mt-2'>
        <p className='fs-4 pt-2 mt-2  text-center'>FactWise Assessment Visual Reference</p>
        <p className='fs-5 pt-3 mt-2 list-para'>List View</p>
        {/* <input type="text" className ="search" placeholder="Search by name" /> */}
        {/* Circular buttons for each user */}
        <UserList onSelect={handleUserSelect} selectedUser={selectedUser} />
        {selectedUser !== null && (
          <div>
            {/* Display open/edit/delete state div based on selectedUser state */}
            {/* Implement UserDetails, UserEdit, and UserDelete components */}
          </div>
        )}
      </div>
    // </div>
  );
};

export default App;